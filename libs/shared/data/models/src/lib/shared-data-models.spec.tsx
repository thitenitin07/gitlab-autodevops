import { render } from '@testing-library/react';

import SharedDataModels from './shared-data-models';

describe('SharedDataModels', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<SharedDataModels />);
    expect(baseElement).toBeTruthy();
  });
});
