import './shared-data-models.module.css';

/* eslint-disable-next-line */
export interface SharedDataModelsProps {}

export function SharedDataModels(props: SharedDataModelsProps) {
  return (
    <div>
      <h1>Welcome to SharedDataModels!</h1>
    </div>
  );
}

export default SharedDataModels;
