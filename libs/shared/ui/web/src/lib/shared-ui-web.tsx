import './shared-ui-web.module.css';

/* eslint-disable-next-line */
export interface SharedUiWebProps {}

export function SharedUiWeb(props: SharedUiWebProps) {
  return (
    <div>
      <h1>Welcome to SharedUiWeb!</h1>
    </div>
  );
}

export default SharedUiWeb;
