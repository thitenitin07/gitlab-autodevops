import { render } from '@testing-library/react';

import SharedUiWeb from './shared-ui-web';

describe('SharedUiWeb', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<SharedUiWeb />);
    expect(baseElement).toBeTruthy();
  });
});
